import React, {useEffect, useState} from 'react';
import {TextField} from "@material-ui/core";
import {Button} from "@material-ui/core";
import './App.css';
import e from './e.json';



const App = () => {

  const eArray = new Array<number>(e.length);
  for (let i = 0; i < e.length; ++i) {
    eArray[i] = parseInt(e.slice(i, i+1));
  }

  const [urE, seturE] = useState('');
  const [toCheck, setToCheck] = useState(false);
  const [showE, setShowE] = useState(false);

  let check = () => {
    setToCheck(true);
    setShowE(false);
  };

  let show = () => {
    setShowE(!showE);
    setToCheck(false);
  };

  useEffect(() => {
    console.log(toCheck)
  }, [toCheck]);

  return (
      <div className="App">
        <header className="App-header">
          <div className="numbers">
            <p className="eText">{showE ? 2.7 + e : 2.7}</p>
            <div className="feedBack">
              {eArray.map((digit, i) => {
                if (toCheck && i < urE.length) {
                  if(digit.toString() === urE.slice(i, i + 1)) {
                    return <p className="correct">{digit}</p>
                  }
                  return <p className="wrong">{digit}</p>
                }
                return null;
              })}
            </div>
            <TextField  id="outlined-basic" label="e = 2.7..." variant="outlined" value={urE} onClick={() => {setToCheck(false);
              setShowE(false)}} onChange={(e) => {seturE(e.currentTarget.value);}}/>
            <Button className="textInput" variant="contained" onClick={() => {check()}}>Check</Button>
            <Button className="textInput" variant="contained" onClick={() => {show()}}>Show e</Button>
          </div>
        </header>
      </div>
  )
};

export default App;
